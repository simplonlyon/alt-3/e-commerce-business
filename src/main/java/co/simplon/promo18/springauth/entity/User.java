package co.simplon.promo18.springauth.entity;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import co.simplon.promo18.springauth.entity.type.OrderStatus;

@Entity
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Email
    @NotBlank
    private String email;
    @NotBlank
    @Size(min=4, max=64)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) //On ignore le password en Json en lecture pour que le mot de passe se balade pas trop
    private String password;
    private String role;
    @OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST)
    @JsonIgnore
    private Set<Order> orders = new HashSet<>();

    /**
     * Méthode qui sera lancé au moment de faire persister un nouveau user,
     * permet de s'assurer que celui ci aura forcément un panier
     */
    @PrePersist
    private void initializeCart() {
        Order cart = new Order();
        cart.setUser(this);
        orders.add(cart);
    }

    /**
     * Dans la plupart des cas, cette méthode pour aller chercher le panier serait ok
     * mais avec JPA il s'avère que ça finit par causer pas mal de soucis à cause de
     * la manière dont il fait le lazyloading
     */
    // @JsonIgnore
    // public Order getCart() {
    //    for (Order order : orders) {
    //         if(order.getStatus() == OrderStatus.CART) {
    //             return order;
    //         }
    //    }
    //    return null;
    // }

    public Set<Order> getOrders() {
        return orders;
    }
    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(
            new SimpleGrantedAuthority(role)
        );
    }
    @Override
    public String getUsername() {
        return email;
    }
    
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
    
}
