package co.simplon.promo18.springauth.dto;

import co.simplon.promo18.springauth.entity.type.SizeLabel;

public class AddCartDto {
    public int idProduct;
    public SizeLabel size;
}
