package co.simplon.promo18.springauth.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.springauth.auth.UserRoles;
import co.simplon.promo18.springauth.entity.User;
import co.simplon.promo18.springauth.repository.UserRepository;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
    @Autowired
    private UserRepository userRepo;

    @GetMapping("/user")
    public List<User> all() {
        
        return userRepo.findAll();            
    
        // throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Vous devez être connecté pour accéder à cette ressource");
    }

    @PatchMapping("/user/{id}/role/{role}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changeRole(@PathVariable int id, @PathVariable UserRoles role) {
        User user = userRepo.findById(id).orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        user.setRole(role.toString());
        userRepo.save(user);
    }
}
