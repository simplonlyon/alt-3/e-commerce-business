package co.simplon.promo18.springauth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.springauth.business.CartBusiness;
import co.simplon.promo18.springauth.business.exception.NotAvailableException;
import co.simplon.promo18.springauth.dto.AddCartDto;
import co.simplon.promo18.springauth.entity.Order;
import co.simplon.promo18.springauth.entity.User;

@RestController
@RequestMapping("/api/cart")
public class CartController {
    @Autowired
    private CartBusiness cartBusiness;
    
    @GetMapping
    public Order cart(@AuthenticationPrincipal User user) {
        return cartBusiness.getCart(user);
    }

    @PostMapping
    public Order addtoCart(@AuthenticationPrincipal User user, @RequestBody AddCartDto addDto) {
        try {
            return cartBusiness.addToCart(user, addDto);
        } catch (NotAvailableException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
            
        }
    }

    @PatchMapping
    public Order validate(@AuthenticationPrincipal User user) {
        try {
            return cartBusiness.validateCart(user);
        } catch (NotAvailableException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
            
        }
    }

}
