package co.simplon.promo18.springauth.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.springauth.auth.ChangePasswordDto;
import co.simplon.promo18.springauth.business.UserBusiness;
import co.simplon.promo18.springauth.business.exception.UserExistsException;
import co.simplon.promo18.springauth.business.exception.WrongPasswordException;
import co.simplon.promo18.springauth.entity.User;
import co.simplon.promo18.springauth.repository.OrderRepository;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserBusiness business;

    @Autowired
    private OrderRepository orderRepo;

    @GetMapping
    public String test(@AuthenticationPrincipal User user) {
        if (user != null) {
            System.out.println(orderRepo.findCart(user).isPresent());
            // System.out.println(user.getCart());
            return "Hello " + user.getEmail();
        }

        return "not connected";
    }

    @GetMapping("/account")
    public User getAccount(@AuthenticationPrincipal User user) {
        return user;
    }

    @PostMapping
    public User register(@Valid @RequestBody User user) {
        try {
            return business.register(user);
        } catch (UserExistsException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PatchMapping("/password")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changePassword(@RequestBody ChangePasswordDto body, @AuthenticationPrincipal User user) {
        try {
            business.changePassword(body, user);
        } catch (WrongPasswordException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Old password doesn't match");

        }
    }

}
