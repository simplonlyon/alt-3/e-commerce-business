package co.simplon.promo18.springauth.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import co.simplon.promo18.springauth.auth.ChangePasswordDto;
import co.simplon.promo18.springauth.auth.UserRoles;
import co.simplon.promo18.springauth.business.exception.UserExistsException;
import co.simplon.promo18.springauth.business.exception.WrongPasswordException;
import co.simplon.promo18.springauth.entity.User;
import co.simplon.promo18.springauth.repository.UserRepository;

@Service
public class UserBusiness {
    
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private PasswordEncoder encoder;

    public User register(User user) throws UserExistsException {

        if(userRepo.findByEmail(user.getEmail()).isPresent()){
            throw new UserExistsException();
        }
        user.setId(null);
        user.setRole(UserRoles.ROLE_USER.name());
        hashPassword(user);

        userRepo.save(user);
        
        connectUser(user);
        

        return user;
    }

    public void changePassword(ChangePasswordDto passwordDto, User user) throws WrongPasswordException {
        if(!encoder.matches(passwordDto.oldPassword, user.getPassword())) {
            throw new WrongPasswordException();
        }
        user.setPassword(passwordDto.newPassword);
        hashPassword(user);
        
        userRepo.save(user);
    }

    private void connectUser(User user) {
        
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()));
    }

    private void hashPassword(User user) {
        String hashed = encoder.encode(user.getPassword());
        user.setPassword(hashed);
    }
}
