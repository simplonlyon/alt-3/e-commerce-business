package co.simplon.promo18.springauth.auth;

public enum UserRoles {
    ROLE_USER,
    ROLE_ADMIN
}
