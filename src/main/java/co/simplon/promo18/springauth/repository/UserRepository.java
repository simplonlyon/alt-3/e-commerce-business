package co.simplon.promo18.springauth.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.springauth.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    
    Optional<User> findByEmail(String email);
}
