package co.simplon.promo18.springauth.entity.type;

public enum SizeLabel {
    XXS,
    XS,
    S,
    M,
    L,
    XL,
    XXL
}
