package co.simplon.promo18.springauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.springauth.entity.Cloth;

@Repository
public interface ClothRepository extends JpaRepository<Cloth, Integer>{
    
}
