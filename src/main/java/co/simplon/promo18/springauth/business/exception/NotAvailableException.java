package co.simplon.promo18.springauth.business.exception;

public class NotAvailableException extends Exception{

    public NotAvailableException(String msg) {
        super(msg);
    }


    
}
