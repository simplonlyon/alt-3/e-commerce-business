package co.simplon.promo18.springauth.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.springauth.entity.Order;
import co.simplon.promo18.springauth.entity.User;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {

    @Query("From Order o WHERE o.status='CART' AND user=:user")
    Optional<Order> findCart(User user);
}
