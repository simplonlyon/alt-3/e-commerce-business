package co.simplon.promo18.springauth.business;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.simplon.promo18.springauth.business.exception.NotAvailableException;
import co.simplon.promo18.springauth.dto.AddCartDto;
import co.simplon.promo18.springauth.entity.Cloth;
import co.simplon.promo18.springauth.entity.Order;
import co.simplon.promo18.springauth.entity.OrderItem;
import co.simplon.promo18.springauth.entity.Size;
import co.simplon.promo18.springauth.entity.User;
import co.simplon.promo18.springauth.entity.type.OrderStatus;
import co.simplon.promo18.springauth.entity.type.SizeLabel;
import co.simplon.promo18.springauth.repository.ClothRepository;
import co.simplon.promo18.springauth.repository.OrderRepository;
import co.simplon.promo18.springauth.repository.UserRepository;

@Service
public class CartBusiness {
    @Autowired
    private OrderRepository orderRepo;

    @Autowired
    private ClothRepository clothRepo;


    public Order getCart(User user) {
        return orderRepo.findCart(user).get();
    }

    public Order addToCart(User user, AddCartDto addDto) throws NotAvailableException {
        Cloth cloth = clothRepo.findById(addDto.idProduct).get();
        Size size = sizeAvailable(cloth, addDto.size, 1);
        if (size == null) {
            throw new NotAvailableException("Size not available");
        }
        Order cart = getCart(user);
        OrderItem item = isInCart(cloth, cart, size);
        if (item == null) {
            item = initItem(cloth, size, cart);
        } else {
            item.setQuantity(item.getQuantity() + 1);
        }
        return orderRepo.save(cart);

    }


    public Order validateCart(User user) throws NotAvailableException {
        Order cart = getCart(user);
        for(OrderItem item: cart.getItems()) {
            if(sizeAvailable(item.getCloth(), item.getSize().getLabel(), item.getQuantity()) == null) {
                throw new NotAvailableException("The cloth "+item.getCloth().getLabel()+" is not available anymore in size "+item.getSize().getLabel());
            }
            item.getSize().setStock(item.getSize().getStock() - item.getQuantity());
        }
        cart.setStatus(OrderStatus.ORDERED);
        cart.setDate(LocalDate.now());
        orderRepo.save(cart);
        Order newCart = new Order();
        newCart.setUser(user);
        orderRepo.save(newCart);
        return newCart;

    }


    // Les méthodes qui suivent pourraient et devraient être faites en TDD, elles s'y prêtent bien
    private OrderItem initItem(Cloth cloth, Size size, Order cart) {
        OrderItem item = new OrderItem();
        item.setOrder(cart);
        item.setCloth(cloth);
        item.setSize(size);
        item.setQuantity(1);
        cart.getItems().add(item);
        return item;
    }


    public Size sizeAvailable(Cloth cloth, SizeLabel size, int quantity) {

        for (Size sizeInCloth : cloth.getSizes()) {
            if (sizeInCloth.getLabel() == size) {
                if (sizeInCloth.getStock() >= quantity) {
                    return sizeInCloth;
                }
            }
        }

        return null;
    }

    public OrderItem isInCart(Cloth cloth, Order cart, Size size) {
        for (OrderItem item : cart.getItems()) {
            if (item.getCloth() == cloth && item.getSize() == size) {
                return item;
            }
        }
        return null;
    }

}
