package co.simplon.promo18.springauth.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import co.simplon.promo18.springauth.entity.type.SizeLabel;

@Entity
public class Size {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private SizeLabel label;
    private Integer stock;
    @ManyToOne
    private Cloth cloth;
    @OneToMany(mappedBy = "size")
    @JsonIgnore
    private Set<OrderItem> orderItem = new HashSet<>();
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public SizeLabel getLabel() {
        return label;
    }
    public void setLabel(SizeLabel label) {
        this.label = label;
    }
    public Integer getStock() {
        return stock;
    }
    public void setStock(Integer stock) {
        this.stock = stock;
    }
    public Cloth getCloth() {
        return cloth;
    }
    public void setCloth(Cloth cloth) {
        this.cloth = cloth;
    }
    public Set<OrderItem> getOrderItem() {
        return orderItem;
    }
    public void setOrderItem(Set<OrderItem> orderItem) {
        this.orderItem = orderItem;
    }
}
