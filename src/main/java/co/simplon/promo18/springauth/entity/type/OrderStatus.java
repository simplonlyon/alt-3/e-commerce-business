package co.simplon.promo18.springauth.entity.type;

public enum OrderStatus {
    CART,
    DELIVERED,
    ORDERED,
    SENT
}
